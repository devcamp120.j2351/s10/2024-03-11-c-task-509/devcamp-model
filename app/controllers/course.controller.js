const mongoose = require('mongoose');
const courseModel = require("../models/course.model");

const createCourse = async (req, res) => {
    try {
        // B1: Chuẩn bị dữ liệu
        // { reqTitle: 'R51', reqDescription: 'React 2351', noStudent: 15 }
        const { reqTitle, reqDescription, noStudent } = req.body;
        
        // B2: Validate dữ liệu
        if(!reqTitle) {
            return res.status(400).json({
                status: "Bad Request",
                message: "Title is required!"
            })
        }

        if( !(Number.isInteger(noStudent) && noStudent >= 0) ) {
            return res.status(400).json({
                status: "Bad Request",
                message: "Count student is not valid!"
            })
        }

        // B3: Thao tác với CSDL
        const newCourse = {
            _id: new mongoose.Types.ObjectId(),
            title: reqTitle,
            description: reqDescription,
            noStudent: noStudent
        }

        const result = await courseModel.create(newCourse);

        return res.status(200).json({
            status: "Create course successfully",
            data: result
        })
   
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}

const getAllCourse = async (req, res) => {
    try {
        // B1: Chuẩn bị dữ liệu
        // B2: Validate dữ liệu
        // B3: Thao tác với CSDL
        const result = await courseModel.find();

        return res.status(200).json({
            status: "Get all course successfully",
            data: result
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}

const getCourseByID = async (req, res) => {
    try {
        // B1: Chuẩn bị dữ liệu
        const courseId = req.params.courseid;

        // B2: Validate dữ liệu
        if( !mongoose.Types.ObjectId.isValid(courseId) ) {
            return res.status(400).json({
                status: "Bad Request",
                message: "Course ID is not valid!"
            })
        }
        // B3: Thao tác với CSDL
        const result = await courseModel.findById(courseId);

        if(result) {
            return res.status(200).json({
                status: "Get course successfully",
                data: result
            })
        } else {
            return res.status(404).json({
                status: "Not Found"
            })
        }

    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}

const updateCourseById = async (req, res) => {
    try {
        // B1: Chuẩn bị dữ liệu
        const courseId = req.params.courseid;
        const { reqTitle, reqDescription, noStudent } = req.body;

        // B2: Validate dữ liệu
        if( !mongoose.Types.ObjectId.isValid(courseId) ) {
            return res.status(400).json({
                status: "Bad Request",
                message: "Course ID is not valid!"
            })
        }

        if( reqTitle !== undefined && !reqTitle ) {
            return res.status(400).json({
                status: "Bad Request",
                message: "Title is required!"
            })
        }

        if( noStudent !== undefined && !(Number.isInteger(noStudent) && noStudent >= 0) ) {
            return res.status(400).json({
                status: "Bad Request",
                message: "Count student is not valid!"
            })
        }

        // B3: Thao tác với CSDL
        var courseUpdate = {};

        if(reqTitle !== undefined) { courseUpdate.title = reqTitle };
        if(reqDescription !== undefined) { courseUpdate.description = reqDescription };
        if(noStudent !== undefined) { courseUpdate.noStudent = noStudent };

        const result = await courseModel.findByIdAndUpdate(courseId, courseUpdate);

        if(result) {
            return res.status(200).json({
                status: "Update course successfully",
                data: result
            })
        } else {
            return res.status(404).json({
                status: "Not Found"
            })
        }

    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}

const deleteCourseById = async (req, res) => {
    try {
        // B1: Chuẩn bị dữ liệu
        const courseId = req.params.courseid;
        // B2: Validate dữ liệu
        if( !mongoose.Types.ObjectId.isValid(courseId) ) {
            return res.status(400).json({
                status: "Bad Request",
                message: "Course ID is not valid!"
            })
        }
        // B3: Thao tác với CSDL
        const result = await courseModel.findByIdAndDelete(courseId);

        if(result) {
            return res.status(200).json({
                status: "Delete course successfully",
                data: result
            })
        } else {
            return res.status(404).json({
                status: "Not Found"
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
}

module.exports = {
    createCourse,
    getAllCourse,
    getCourseByID,
    updateCourseById,
    deleteCourseById
}