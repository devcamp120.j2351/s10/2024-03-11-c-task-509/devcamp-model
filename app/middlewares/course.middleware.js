const CourseCommon = (req, res, next) => {
    console.log(`Course Middleware: Time: ${new Date()} - Method: ${req.method} - URL: ${req.url}`);

    next();
}

const GetAllCourse = (req, res, next) => {
    console.log('Get All Course Middleware');

    next();
}

const GetCourseByID = (req, res, next) => {
    console.log('Get A Course By ID Middleware');

    next();
}

const CreateCourse = (req, res, next) => {
    console.log('Create New Course Middleware');

    next();
}

const UpdateCourseByID = (req, res, next) => {
    console.log('Update Course By ID Middleware');

    next();
}

const DeleteCourseByID = (req, res, next) => {
    console.log('Delete Course By ID Middleware');

    next();
}
module.exports = {
    CourseCommon,
    GetAllCourse,
    GetCourseByID,
    CreateCourse,
    UpdateCourseByID,
    DeleteCourseByID
}