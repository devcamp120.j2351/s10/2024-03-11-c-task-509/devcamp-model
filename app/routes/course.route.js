//khai báo thư viện express
const express = require('express')

//khai báo router
const router = express.Router();

//import middleware
const courseMiddleware = require('../middlewares/course.middleware')

const courseController = require("../controllers/course.controller");

router.use(courseMiddleware.CourseCommon);

//khai báo các request
//get all course
router.get('/', 
    courseMiddleware.GetAllCourse, 
    courseController.getAllCourse)

//get a couse by id
router.get('/:courseid', 
    courseMiddleware.GetCourseByID, 
    courseController.getCourseByID)

//create new course
router.post('/', 
    courseMiddleware.CreateCourse, 
    courseController.createCourse)

//update course by id
router.put('/:courseid', 
    courseMiddleware.UpdateCourseByID, 
    courseController.updateCourseById)

//delete course by id
router.delete('/:courseid', 
    courseMiddleware.DeleteCourseByID, 
    courseController.deleteCourseById)


module.exports = router;